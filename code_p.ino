/* -------- Liste des includes -------- */
#include <Servo.h> // Pour le servo moteur
#include "grove_two_rgb_led_matrix.h" // Pour la led RDB
#include <SD.h>  // Pour la communication avec la carte SD
#include "WiFiEsp.h" // Pour la carte Wi-Fi

/* -------- Site web adresse -------- */
// 192.168.1.10:8080

/* -------- Création des objests pour contrôler les bibliothèques -------- */
Servo myServo; 
GroveTwoRGBLedMatrixClass matrix;
SDClass;
File RootFile;
File MonFichier;
WiFiEspClient client;

/* -------- Pour la partie initialisation du LED RDG -------- */
#ifdef ARDUINO_SAMD_VARIANT_COMPLIANCE
#define SERIAL SerialUSB
#else
#define SERIAL Serial
#endif

/* -------- Initialisation -------- */
#define init_db 115200
#define CAM_SERIAL Serial2

/*
  Listes des broches pour chaque capteur:
     D2 -> Led
     D4 -> Carte SD
     D5 -> Servo
     D6 -> Bouton poussoir
     D8/D9 -> Télémètre US
     I2C ->   Carte LED RGB
     UART3 -> Carte wifi
     UART2 -> Camera
*/

/* ------------- Constantes des broches ------------- */

// Definir la led
const int ledPin = 2;
// Definir buttonPin pour activer via un bouton
const int buttonPin = 6;
// Definir attach_servoPin pour le servomoteur
const int attach_servoPin = 5;
// Definir echoPin et trigPin pour le capteur à ultrason
const int echoPin = 8;
const int trigPin = 9;
// Definir carte_sdPin pour la carte SD
const byte carte_sdPin = 4;

/* ------------- Variables ------------- */

// Variables pour le bouton
bool state_system = false;
bool state_button = false;
// Variables pour le servomoteur
int angle;
int direction = 1;
// Variables pour la durée et la distance
long duree;
float distance;
//Initialisation pour la matrice pour le RGB led 8*8
orientation_type_t value = DISPLAY_ROTATE_270;
//Variable pour nommer image de la camera
char image_name [] = "IMAGEX.jpg";
// Initialisation WiFi 
char ssid[] = "CameraTP";        // nom du réseau SSID
char pass[] = "CameraTP";        // mot de passe du réseau SSID
int status = WL_IDLE_STATUS;     // l'état de la radio WiFi
char server[] = "192.168.1.10";  // IP du serveur
//long int mytime;                 // répéter la commande
//long int timeout = 5000;         // chaque 5 secondes

/* ------------- Motifes RGB LED ------------- */

/* Croix rouge motif par défault quand l'obejt n'est pas en fonctionnement*/
uint8_t pic8[] = {red, black, black, black, black, black, black, red,
                  black, red, black, black, black, black, red, black,
                  black, black, red, black, black, red, black, black,
                  black, black, black, red, red, black, black, black,
                  white, white, white, red, red, white, white, white,
                  white, white, red, white, white, red, white, white,
                  white, red, white, white, white, white, red, white,
                  red, white, white, white, white, white, white, red,
                 };

/* Validé vert motif par défault s'il n'y a pas une personne à - 100 cm */
uint8_t pic8b[] = {white, white, white, white, white, white, white, white,
                   green, white, white, white, white, white, white, green,
                   green, green, white, white, white, white, green, green,
                   green, green, green, white, white, green, green, green,
                   black, green, green, white, white, green, green, black,
                   black, black, green, green, green, green, black, black,
                   black, black, black, green, green, black, black, black,
                   black, black, black, black, black, black, black, black,
                  };

/* Validé rouge motif s'il y a une personne à - 100 cm */
uint8_t pic8bb[] = {black, black, black, black, black, black, black, black,
                    red, black, black, black, black, black, black, red,
                    red, red, black, black, black, black, red, red,
                    red, red, red, black, black, red, red, red,
                    white, red, red, black, black, red, red, white,
                    white, white, red, red, red, red, white, white,
                    white, white, white, red, red, white, white, white,
                    white, white, white, white, white, white, white, white,
                   };

/* ---------- SEPARATEUR DE PARTIE DE CODE ---------- */

/* ---------- PARTIE ----- INITALISATION ---------- */

void init_led() {
  pinMode(ledPin, OUTPUT); // Initialise la broche 2 comme sortie
  digitalWrite(ledPin, LOW); // ledPin = 2 au niveau bas = éteint la LED
}

void init_bouton() {
  pinMode(buttonPin, INPUT_PULLUP); //  Initialise la broche numérique 6 en tant qu'entrée avec la résistance pull-up interne activée surveiller l'état du bouton
}

void init_servo() {
  myServo.attach(attach_servoPin); // Définit sur quelle broche est fixé le servomoteur
  myServo.write(10); // Mettre en position par défault le servo
}

void init_matrix() {
  Wire.begin(); // Syntaxe pour initialiser la matrice
  delay(1000); // Suspend le programme pendant la durée
  matrix.setDisplayOrientation(value); // Permet de fixer l'angle d'affichage a la value qui vaut 270
}

void init_ultra() {
  pinMode(echoPin, INPUT); // Initialise la broche 8 comme entrée
  pinMode(trigPin, OUTPUT); // Initialise la broche 9 comme sortie
}

void init_camera() {
  char sync[] =  {0xaa, 0x0d, 0x00, 0x00, 0x00, 0x00}; // initialisation sync
  char rep_sync[] =  {0xaa, 0x0e, 0x0d, 0x00, 0x00, 0x00};  // initialisation reponse du sync
  char initial[] = {0xaa, 0x01, 0x00, 0x07, 0x00, 0x07}; // inialisation 
  char snapshot_compressed [] = {0xaa, 0x05, 0x00, 0x00, 0x00, 0x00};
  char set_package_size[] = {0xaa, 0x06, 0x08, 0x00, 128, 0x00};

  unsigned char resp[6];  // initialisation resp -> respond

  //CAM_SERIAL.setTimeout(100);
  /* La première commande envoyée à la camera est une commande SYNC */
  while (1) { // Boucle inifi
    for (int i = 0; i < 6; i++) {
      CAM_SERIAL.print(sync[i]);
    }
    if ((CAM_SERIAL.readBytes((char*)resp, 6) != 6)) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire
      continue; // Ignore le reste de l'itération en cours de la condition
    }
    if (resp[0] == 0xaa && resp[1] == 0x0e && resp[2] == 0x0d && resp[4] == 0 && resp[5] == 0) {
      if (CAM_SERIAL.readBytes((char*)resp, 6) != 6) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire
        continue; // Ignore le reste de l'itération en cours de la condition
      }
      if (resp[0] == 0xaa && resp[1] == 0x0d && resp[2] == 0 && resp[3] == 0 && resp[4] == 0 && resp[5] == 0) {
        break; // Sortir de la condition
      }
    }
  }
  for (int i = 0; i < 6; i++) {
    CAM_SERIAL.print(rep_sync[i]);
  }

  /* La deuxième commande envoyée à la camera est une commande INITIAL avec les infos de couleur, taille et résolution des images */
  while (1) { // Boucle inifi
    for (int i = 0; i < 6; i++) {
      CAM_SERIAL.print(initial[i]);
    }
    if ((CAM_SERIAL.readBytes((char*)resp, 6) != 6)) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire
      continue; // Ignore le reste de l'itération en cours de la condition
    }
    if (resp[0] == 0xaa && resp[1] == 0x0e && resp[2] == 0x01 && resp[4] == 0 && resp[5] == 0) {
      break; // Sortir de la condition
    }
  }

  /* Permettre la récupération des paquets demandés */
  while (1) { // Boucle inifi
    for (int i = 0; i < 6; i++) {
      CAM_SERIAL.print(set_package_size[i]);
    }
    if ((CAM_SERIAL.readBytes((char*)resp, 6) != 6)) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire
      continue; // Ignore le reste de l'itération en cours de la condition
    }
    if (resp[0] == 0xaa && resp[1] == 0x0e && resp[2] == 0x06 && resp[4] == 0 && resp[5] == 0) {
      break; // Sortir de la condition
    }
  }
  /* Récuperer de manière instantané compressé les images */
  while (1) { // Boucle inifi
    for (int i = 0; i < 6; i++) {
      CAM_SERIAL.print(snapshot_compressed[i]);
    }
    if ((CAM_SERIAL.readBytes((char*)resp, 6) != 6)) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire
      continue; // Ignore le reste de l'itération en cours de la condition
    }
    if (resp[0] == 0xaa && resp[1] == 0x0e && resp[2] == 0x05 && resp[4] == 0 && resp[5] == 0) {
      break; // Sortir de la condition
    }
  }
  Serial.println("Fin initialisation camera");
}

void init_sd_cart() {
  pinMode(carte_sdPin, OUTPUT);  // Initialise la broche 4 comme sortie
  /* Initialisation de la carte SD */
  Serial.print("Init SD card... ");
  if (!SD.begin(carte_sdPin)) { // Verifier que la carte SD est bien 
    Serial.println("FAIL SD card"); // Si la SD cart n'est pas valide alors on fail
  }
  RootFile = SD.open("/"); //Ouvre un fichier sur la carte SD. Si le fichier est ouvert en écriture, il sera créé s'il n'existe pas déjà (mais le répertoire le contenant doit déjà exister).
  printDirectory(RootFile, 0); // Affiche le contenue du repertoire pour la table 0

  /*
    File fichier = SD.open("toto.txt"); // Ouvrir toto.txt
    Serial.print("Taille toto.txt : ");
    Serial.print(fichier.size()); // Afficher la taille de toto

    if(SD.exists("test")) { // Si le dossier test exist
      SD.rmdir("test"); // On le supprime
    } else {
      Serial.println("KO technique du dossier"); // Si on relance et qu'il est supprimer on affiche le message
    }

    if(SD.exists("image.jpg")) { // Si le fichier test exist
    SD.remove("image.jpg"); // On le supprime
    } else {
      Serial.println("KO technique des fichiers"); // Si on relance et qu'il est supprimer on affiche le message
    }
  */
}

/* Fonction qui a mon avis est inutile sauf si on veut affichier le repertoire */
void printDirectory(File dir, int numTabs) {
  while (true) {
    File entry = dir.openNextFile(); // En entrée on ore
    if (! entry) {
      // Plus de fichier
      break; // Sortir de la condition
    }
    for (uint8_t i = 0; i < numTabs; i++) { // uint8_t = unsigned int donc 8 bits
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) { // Savoir si l'entrée est un repertoire
      Serial.println("/");
      printDirectory(entry, numTabs + 1); // Afficher le repertoire
    } else {
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

void init_wifi(){
  WiFi.init(&Serial3); // Initialiser ESP module

  // Vérfifier la présence de la cartes conçu pour se poser sur l'arduino ( sert à verifier le branchement )
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // ne pas continuer
    while (true);
  }

  // Tentative de connexion au réseau
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connection au réseau WPA/WPA2 :
    /* WPA et WPA2 sont les mesures de sécurité les plus courantes utilisées pour protéger l’internet sans fil.*/
    status = WiFi.begin(ssid, pass);
  }

  // Une fois connecté maintenant, imprimer l'état du WiFi
  Serial.println("You're connected to the network");
  printWifiStatus();
  makerequest();
}

void printWifiStatus(){
  // Imprimer le SSID du réseau auquel vous êtes connecté
  /* Le SSID (Service Set IDentifier) est tout simplement le nom d'un réseau WiFi, composé au maximum de 32 caractères alphanumériques */
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // Imprimer l'adresse IP du shield WiFi 
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // Imprimer la force du signal reçu Responsable de la Sécurité des Systèmes d'Information
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void makerequest() { // Fonction pour faire une nouvelle requête
  Serial.println();
  Serial.println("Starting connection to server...");
  // Afficher la connexion obtenue via série
  // Se connecte à une adresse IP et un port spécifiés. La valeur de retour indique le succès ou l'échec
  if (client.connect(server, 8080)) {
    Serial.println("Connected to server");
    // Faire une requete HTTP 
    client.print(
      "POST /api HTTP/1.1\r\n"
      "Host: 192.168.1.10:8080\r\n"
      "Content-Type: Binary\r\n"
      "Content-Length: 5\r\n"
      "Connection: close\r\n"
      "\r\n"
      );

    unsigned char bit_angle[] = {angle};
    client.write(bit_angle,1); // Écrire des données sur le serveur auquel le client est connecté 1 pour 1 octets
    unsigned char bit_distance[sizeof(distance)];
    memcpy(bit_distance,&distance,sizeof(distance)); // Copier le contenu d'une variable dans une autre
    client.write(bit_distance, 4); // Écrire des données sur le serveur auquel le client est connecté 4 pour 4 octets
    client.print("\r\n\r\n");
    Serial.println("Disconnecting from server...");
    client.stop(); // Se déconnecte du serveur
  }
}

/* ---------- SEPARATEUR DE PARTIE DE CODE ---------- */

/* ---------- PARTIE ----- RUN ---------- */

void run_led() {
  analogWrite(ledPin, 200); // Pour allumer une LED à différentes luminosités ici 200 ( valeur entre 0 et 255 )
  digitalWrite(ledPin, HIGH); // Met la broche 2 au niveau haut = allume la LED
  delay(100); // Suspend le programme pendant la durée ici 100 (en millisecondes) 
}

void run_servo() {
  /* Permet que le servomoteur ne s'arrete pas une fois lancé */
  angle = myServo.read(); // Lit la valeur courant du servomoteur
  if (direction == 1) { // Direction == 1 permet de faire démarrer le servomoteur jusqu'a 160
    angle += 5.00; // Augmentation de l'angle de 5 en 5
    myServo.write(angle); // Ici on fixe la valeur de l'angle lu
    if (angle >= 160) {
      direction = 0; // Si on arrive à 160 on pars vers 0
    }
  } else if (direction == 0) {
    angle -= 5.00; // Diminution de l'angle de 5 en 5
    myServo.write(angle);
    if (angle <= 5.00) { // Si on arrive à la valeur min 5 
      direction = 1; // On retourne vers la valeur max 160
    }
  }
}

void run_matrix_green() {
  matrix.displayFrames(pic8b, 2000, true, 1); // Affichage du model
  delay(50); //Suspend le programme pendant la durée
}

void run_matrix_red() { // Affichage du model
  matrix.displayFrames(pic8, 2000, true, 1);
  delay(50); //Suspend le programme pendant la durée
}

void run_matrix_activite() { // Affichage du model
  matrix.displayFrames(pic8bb, 2000, true, 1);
  delay(50); //Suspend le programme pendant la durée
}

void run_activite() { 
  if (distance < 100) {  
    run_matrix_activite(); 
    Serial.println("Attention il y a quelqu'un à moins de 100 cm"); 
    delay(100); 
    makerequest(); 
  }
  delay(100); 
}

void run_ultrason() {
  digitalWrite(trigPin, LOW); // Désactive la broche numérique 
  delayMicroseconds(5); //Suspend le programme pendant la durée
  digitalWrite(trigPin, HIGH); // Activé la broche numérique 
  delayMicroseconds(10); //Suspend le programme pendant la durée
  digitalWrite(trigPin, LOW); // Désactive la broche numérique 
  distance = calculateDistance();// Appelle la fonction de calcul de la distance mesurée par le capteur Ultrasons pour chaque degré
  Serial.print("La distance est de : ");
  Serial.print(distance); // Affichier la distance calculer grâce à formule
  Serial.print(" cm \n");
}

int calculateDistance() {
  digitalWrite(trigPin, LOW); // Désactive la broche numérique 
  delay(30); //Suspend le programme pendant la durée
  // Définit le trigPin sur l'état HIGH pendant 30 secondes
  digitalWrite(trigPin, HIGH); // Activé la broche numérique 
  delay(30); //Suspend le programme pendant la durée
  digitalWrite(trigPin, LOW); // Désactive la broche numérique 
  duree = pulseIn(echoPin, HIGH); // Lit l'echoPin, renvoie le temps de parcours de l'onde sonore en secondes
  distance = duree * 0.034 / 2; // Formule pour le calcule de distance
  return distance;
}

void run_camera() {
  char get_snapshot_picture[] = {0xaa, 0x04, 0x01, 0x00, 0x00, 0x00};
  int LB2;
  int MB2;
  int HB2;
  char ack_id[] = {0xaa, 0x0e, 0x00, 0x00, 0x00, 0x00};
  unsigned char resp[6];
  unsigned char resp2[126];
  unsigned char resp3[122];

  Serial.println("debut partie 1");
  /* La récupération de l'image se fait en deux parties: l'envoie de get_snapshot_picture et la réponse de la caméra avec ack_id et deuxième partie le contact quotidient avec la camera et son environnement */
  /* Première partie */
  while (1) { // Boucle inifi
    for (int i = 0; i < 6; i++) {
      CAM_SERIAL.print(get_snapshot_picture[i]);
    }
    if ((CAM_SERIAL.readBytes((char*)resp, 6) != 6)) { // Lit les caractères du port série dans un tampon, fin si la longueur déterminée a été lue ou si elle expire 
      continue; // Ignore le reste de l'itération en cours de la condition
    } 
    if (resp[0] == 0xaa && resp[1] == 0x0e && resp[2] == 0x04 && resp[4] == 0 && resp[5] == 0) {
      break; // Sortir de la condition
    }
  }
  Serial.println("Fin partie 1 "); 
  /* Deuxième partie */
  while (1) { // Boucle inifi
    if (resp[0] == 0xaa && resp[1] == 0x0a && resp[2] == 0x01) {
      LB2 = resp[3];
      MB2 = resp[4];
      HB2 = resp[5];
      unsigned int image_size = HB2 * pow(2, 16) + MB2 * pow(2, 8) + LB2; // formule de la taille de l'image
      break;
    }
    Serial.println("Fin partie 2 "); 
    unsigned int image_size;
    unsigned char right_part;
    unsigned char left_part;
    int number_trame = image_size / 122;
    /*prendre toute trame*/
    if((image_size %122)!=0){
      number_trame = number_trame +1;
    }
    Serial.println("Recup trame");
    /* Procedure pour nommer les images */
    int i = 0;
    do{
      image_name[5] = '0' + i;
      i++;
      Serial.println(image_name);
    }while(i<10);

    MonFichier = SD.open(image_name, FILE_WRITE);
    for (int i = 0x00; i <= number_trame; i++) {
      right_part = i & (0xFF);
      //Serial.print("right_part = ");
      //Serial.print(right_part, HEX);
      /* La partie dite de gauche est remplie part la partie qui était remplie à droite */
      left_part = (i >> 8) & (0xFF);
      //Serial.print(" |left_part = ");
      //Serial.println(left_part, HEX);
      //Serial.println(i, HEX);
      for (int i = 0; i < 6; i++) {
        CAM_SERIAL.print(ack_id[i]);
      }
      
      int trame_traite = CAM_SERIAL.readBytes((char *)resp2, 128);
      MonFichier.write(&resp2[4],trame_traite-6);
    }
  }
  MonFichier.close();
  Serial.println("Done image");
}

void run_wifi(){
  // s'il y a des octets entrants disponibles depuis le serveur, les lire et les imprimer
  // Devrait être inutile mais si n'y est pas le servomoteur s'arrete
    while (client.available()) { // Nombre d'octets disponibles pour la lecture à partir du port série
                                // Il s'agit de données déjà arrivées et stockées dans le tampon de réception série
      char c = client.read();
      Serial.write(c);
    }
}


/* ------------- Déclaration du VOID SETUP ------------- */

void setup() {
  Serial.begin(init_db); // initialisation des db
  Serial3.begin(init_db);
  CAM_SERIAL.begin(init_db);
  init_led(); // initialisation de la led
  init_bouton(); // initialisation du bouton
  init_servo(); // initialisation du servomoteur
  init_matrix(); // initialisation de la RGB led 8*8
  init_ultra(); // initialisation de l'ultrason
  //init_camera(); // initialisation de la camera
  init_sd_cart(); // initialisation de la carte sd
  init_wifi(); // initialisation de la wifi

}

/* ------------- Déclaration du VOID LOOP ------------- */

void loop() {
  run_led(); // Allumer la led
  /* Partie pour démarer le bouton et dans le système */
  /* Dans IF on retrouve là partie lorsque le bouton n'est pas activé */
  if (!state_system) {
    run_matrix_red();  // Afficher le motif d'inactivité
    while (!digitalRead(buttonPin)) { // Lire l'état du bouton
      delay(10); //Suspend le programme pendant la durée
      state_system = !state_system;
    }
    /* Dans ELSE on retrouve là partie lorsque le bouton est activé */
  } else {
    run_matrix_green(); // Afficher le motif d'activité
    run_servo();  // Allumer le servomoteur
    run_ultrason(); // Allumer l'ultrason
    //run_camera();
    run_activite(); // Motif et envoie de données si < 100 cm
    run_wifi(); // Envoie des données
  }
}
